<?php

namespace Mediapress\Content;

use Illuminate\Support\ServiceProvider;


class ContentServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        //
    }
}
